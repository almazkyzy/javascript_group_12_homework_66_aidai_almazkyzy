import { Injectable } from '@angular/core';
import { CountryService } from '../shared/country.service';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Country } from '../shared/country.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryResolverService implements Resolve<Country> {
  constructor(private countryService: CountryService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country> {
    const id = <string>route.params['alpha3Code'];
    return this.countryService.fetchCountry(id);
  }
}
