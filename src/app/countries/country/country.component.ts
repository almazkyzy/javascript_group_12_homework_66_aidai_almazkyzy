import { Component, OnInit } from '@angular/core';
import { Country } from '../../shared/country.model';
import { ActivatedRoute } from '@angular/router';
import { CountryService } from '../../shared/country.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {
  country!: Country;
  constructor(private countryService: CountryService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data
      .subscribe(data => {
        this.country = <Country>data.country;
      });
  }

}
