import { Component, OnInit, OnDestroy } from '@angular/core';
import { Country } from '../shared/country.model';
import { CountryService } from '../shared/country.service';
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit, OnDestroy {
  countries: Country[] = [];
  countriesFetching = false;
  countriesChangeSubscription!: Subscription;
  countriesFetchingSubscription!: Subscription;
  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
    this.countries = this.countryService.getCountries();
    this.countriesChangeSubscription = this.countryService.countriesChange.subscribe((countries: Country[]) => {
      this.countries = countries;
    });
    this.countriesFetchingSubscription = this.countryService.countriesFetching.subscribe(isFetching => {
      this.countriesFetching = isFetching;
    });
    this.countryService.fetchCountries();
  }

  ngOnDestroy() {
    this.countriesChangeSubscription.unsubscribe();
    this.countriesFetchingSubscription.unsubscribe();
  }
}
