import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryResolverService } from './countries/country-resolver.service';
import { CountriesComponent } from './countries/countries.component';
import { CountryComponent } from './countries/country/country.component';

const routes: Routes = [
  {
    path: '',
    component: CountriesComponent
  },
  {
    path: 'countries',
    component: CountriesComponent, children: [
      {
        path: ':alpha3Code',
        component: CountryComponent,
        resolve: {
            country: CountryResolverService
        }
      }
    ]
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
