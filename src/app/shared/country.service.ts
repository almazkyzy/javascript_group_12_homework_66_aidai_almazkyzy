import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Country } from './country.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable()

export class CountryService {
  countriesChange = new Subject<Country[]>();
  countriesFetching = new Subject<boolean>();

  private countries: Country[] = [];

  constructor(private http: HttpClient) {}

  fetchCountries() {
    this.countriesFetching.next(true);
    this.http.get<{ [id: string]: Country }>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const countryData = result[id];
          console.log(countryData)
          const country = new Country(
            id,
            countryData.name,
            countryData.capital,
            countryData.flag,
          );
          return country;
        });
      }))
      .subscribe(countries => {
        this.countries = countries;
        // console.log(this.countries[0])
        this.countriesChange.next(this.countries.slice());
        this.countriesFetching.next(false);
      }, error => {
        this.countriesFetching.next(false);
      });
  }

  getCountries() {
    return this.countries.slice();
  }

  fetchCountry(alpha3Code: string) {
    return this.http.get<Country>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${alpha3Code}`).pipe(
      map(result => {
        return new Country(
          alpha3Code, result.name, result.capital,
          result.flag
        );
      })
    );
  }
}
